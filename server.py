#!/usr/bin/env python

import os
from os import path
import socket
import struct
import sys
from threading import Lock, Thread
from socket import error as SocketError
import enum

import time

QUEUE_LENGTH = 10
SEND_BUFFER = 4096
threads = []
songlist = []
soundPath = ""
music_path = ""

def songlistEncode(songlist):
    s = '['
    for song in songlist:
        s = s + str(song) + '\t'
    s = s.strip('\t') + ']\r\n'
    return s

class ClientState(enum.Enum):
    Listening = 0
    WantQuit = 1
    WantPlay = 2
    WantStop = 3
    WantList = 4
    RecvUnknown = 5    
    RecvUnWrongIndex = 6

# per-client struct
class Client:
    def __init__(self, connection=None, id=-1):
        self.lock = Lock()
        self.id = id
        self.connection = connection
        self.state = ClientState.Listening
        self.data = ""

    def addDataPrefix(self):
        self.lock.acquire()
        self.data = "play\n" + self.data
        self.lock.release()

    def sendMsg(self, msg):
        self.lock.acquire()
        msgTosent = msg
        while msgTosent:
            try:
                bytesSent = self.connection.send(msgTosent[:SEND_BUFFER])
                msgTosent = msgTosent[bytesSent:]
            except:
                return -1
        self.lock.release()
        print("sent msg:" + msg)
        return 1

    def setState(self, sendMsgReturn):
        self.lock.acquire()
        if sendMsgReturn < 0:
            self.state = ClientState.WantQuit
        else:
            self.state = ClientState.Listening
        self.lock.release()

    def handlePlay(self):
        while self.data:
            self.lock.acquire()
            if self.state == ClientState.WantList:
                self.lock.release()
                self.handleList(stateReset = ClientState.WantPlay)

            elif self.state == ClientState.RecvUnWrongIndex:
                self.lock.release()
                self.handleRecvUnWrongIndex()

            elif self.state == ClientState.RecvUnknown:
                self.lock.release()
                self.handleRecvUnknown(stateReset = ClientState.WantPlay)

            elif self.state == ClientState.WantStop:
                self.lock.release()
                self.handleStop()
                return

            elif self.state == ClientState.WantQuit:
                return
                
            elif self.state == ClientState.WantPlay:
                try:
                    bytesSent = self.connection.send(self.data[:SEND_BUFFER])
                    self.data = self.data[bytesSent:]
                except SocketError:
                    self.lock.release()
                    self.state == ClientState.WantQuit
                    return
                self.lock.release()

        self.state = ClientState.Listening
        print('  client ' + str(self.id) + ": Data has been sent")
    
    def handleStop(self):
        msg = "+OK stop\n"
        self.setState(self.sendMsg(msg))
        
        self.lock.acquire()
        self.data = ""
        self.state = ClientState.Listening
        self.lock.release()
        print('  client ' + str(self.id) +": Data sending stopped")

    def handleList(self, stateReset = ClientState.Listening):
        songlistStr = songlistEncode(songlist)
        msg = "list\n" + songlistStr
        self.setState(self.sendMsg(msg))
        self.state = stateReset
        print('  client ' + str(self.id) +": list sent")

    def handleRecvUnknown(self, stateReset = ClientState.Listening):
        msg = "-ER Unknown command\n"
        self.setState(self.sendMsg(msg))
        self.state = stateReset
        print('  client ' + str(self.id) +": unknown sent processed")
    
    def handleRecvUnWrongIndex(self, stateReset = ClientState.Listening):
        msg = "-ER Wrong Index\n"
        self.setState(self.sendMsg(msg))
        self.state = stateReset
        print('  client ' + str(self.id) +": Wrong Index sent processed")

# TODO: Thread that sends music and lists to the client.  All send() calls
# should be contained in this function.  Control signals from client_read could
# be passed to this thread through the associated Client object.  Make sure you
# use locks or similar synchronization tools to ensure that the two threads play
# nice with one another!

def client_write(client):
    while True:
        if client.state.value == 0:
            continue
        
        client.lock.acquire()
        if client.state == ClientState.WantQuit:
            client.lock.release()
            break

        elif client.state == ClientState.WantStop:
            client.lock.release()
            client.handleStop()
        
        elif client.state == ClientState.WantList:
            client.lock.release()
            client.handleList()

        elif client.state == ClientState.RecvUnknown:
            client.lock.release()
            client.handleRecvUnknown()
            
        elif client.state == ClientState.RecvUnWrongIndex:
            client.lock.release()
            client.handleRecvUnWrongIndex()

        elif client.state == ClientState.WantPlay:
            client.lock.release()
            client.handlePlay()
        
        

    print('client ' + str(client.id) + ' write thread closed')
    print('client ' + str(client.id) + ' quitted')

# TODO: Thread that receives commands from the client.  All recv() calls should
# be contained in this function.
def client_read(client):
    while True:
        # if client.state.value > 0:
        #     continue

        # client.lock.acquire()
        try:
            client_msg = client.connection.recv(SEND_BUFFER)
        except SocketError:
            client.state = ClientState.WantQuit
            break
        if len(client_msg) == 0:
            client.state = ClientState.WantQuit
            break
        # client.lock.release()
        client_msg = client_msg[:(len(client_msg)-2)]
        print('  client ' + str(client.id) + " Message received: " + client_msg)

        cmd = ""
        args = ""
        if ' ' in client_msg:
            cmd, args = client_msg.split(' ', 1)
        else:
            cmd = client_msg
        
        client.lock.acquire()
        if cmd == 'list':
            client.state = ClientState.WantList

        elif cmd == 'quit':
            client.state = ClientState.WantQuit
            client.lock.release()
            break

        elif cmd == 'stop':
            client.state = ClientState.WantStop

        elif cmd == 'play':
            if args.isdigit():
                if int(args) - 1 < len(songlist):
                    music_path = soundPath + '/' + songlist[int(args) - 1]
                    f = open(music_path, 'r')
                    client.data = f.read()
                    client.state = ClientState.WantPlay
                else:
                    client.state = ClientState.RecvUnWrongIndex

        else:
            client.state = ClientState.RecvUnknown
        
        client.lock.release()

    print('client ' + str(client.id) + ' read thread closed')


def get_mp3s(musicdir):
    print("Reading music files...")
    songs = []

    for filename in os.listdir(musicdir):
        if not filename.endswith(".mp3"):
            continue
        else:
            songs.append(filename)

    print("Found {0} song(s)!".format(len(songs)))
    return len(songs), songs
    
def main():
    if len(sys.argv) != 3:
        sys.exit("Usage: python server.py [port] [musicdir]")
    if not os.path.isdir(sys.argv[2]):
        sys.exit("Directory '{0}' does not exist".format(sys.argv[2]))

    global songlist
    global threads
    global soundPath

    port = int(sys.argv[1])
    songs, songlist = get_mp3s(sys.argv[2])
    soundPath = sys.argv[2]
    threads = []

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('0.0.0.0', port))
    s.listen(QUEUE_LENGTH)

    client_id = 1
    while True:
        connection, _ = s.accept()
        client = Client(connection, client_id)
        print("\nclient " + str(client_id) + " connected")
        t = Thread(target=client_read, args=(client,))
        threads.append(t)
        t.start()
        t = Thread(target=client_write, args=(client,))
        threads.append(t)
        t.start()
        client_id += 1
        for t in threads:
            if not t.is_alive():
                threads.remove(t)
    s.close()


if __name__ == "__main__":
    main()